import { Injectable, NotFoundException } from '@nestjs/common';
import { User } from '@prisma/client';
import { PrismaService } from '../prisma/prisma.service';

@Injectable()
export class UserService {
    constructor(private prisma: PrismaService) {}

    async getUserFiles(user: User) {
        let logFiles = await this.prisma.logFile.findMany({
            where: {
                userId: user.id,
            }
        });

        if(!logFiles) 
            throw new NotFoundException('Either user doesn`t exist or there are no files found');

        logFiles.forEach(file => {
            delete file.hash;
            delete file.id;
            delete file.userId;
        });
        
        return {
            files: logFiles,
        }
    }
}
