import { ForbiddenException, Injectable } from "@nestjs/common";
import { PrismaService } from "../prisma/prisma.service";
import { AuthDto } from "./dto";
import * as argon from 'argon2';
import { PrismaClientKnownRequestError } from "@prisma/client/runtime";
import { JwtService } from "@nestjs/jwt";
import { ConfigService } from "@nestjs/config";

const JWT_TOKEN_EXPIRATION = '30m'; // After 15 minutes users need to relog in

@Injectable()
export class AuthService {
    constructor(private prisma: PrismaService, private jwt: JwtService, private config: ConfigService) {}

    async signup(dto: AuthDto) {
        const hash = await argon.hash(dto.password);

        try {
            const user = await this.prisma.user.create({
                data: {
                    email: dto.email,
                    hash: hash,
                },
            });

            return this.signToken(user.id, user.email);
        }
        catch(error) {
            if(error instanceof PrismaClientKnownRequestError) {
                if(error.code === 'P2002') {
                    throw new ForbiddenException('Credentials taken');
                }
            }
            throw error;
        }
    }

    async signin(dto: AuthDto) {
        const user = await this.prisma.user.findUnique({
            where: {
                email: dto.email,
            },
        });

        if(!user) throw new ForbiddenException('Credentials incorrect');

        const passwordsMatch = await argon.verify(user.hash, dto.password); 
        if (!passwordsMatch) throw new ForbiddenException('Passwords don`t match');

        return this.signToken(user.id, user.email);
    }

    async signToken(userId: number, email: string): Promise<{accessToken: string}> {
        const payload = {
            sub: userId,
            email,
        }

        const secret = this.config.get('JWT_SECRET');

        const token = await this.jwt.signAsync(payload, {
            expiresIn: JWT_TOKEN_EXPIRATION,
            secret: secret,
        });

        return {
            accessToken: token,
        }
    }
}