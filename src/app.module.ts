import { Module } from '@nestjs/common';
import { AuthModule } from './auth/auth.module';
import { UserModule } from './user/user.module';
import { PrismaModule } from './prisma/prisma.module';
import { ConfigModule } from '@nestjs/config';
import { FileParserModule } from './file-parser/file-parser.module';
import { FileOutputModule } from './file-output/file-output.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }), 
    AuthModule, UserModule, PrismaModule, FileParserModule, FileOutputModule
  ],
})
export class AppModule {}
