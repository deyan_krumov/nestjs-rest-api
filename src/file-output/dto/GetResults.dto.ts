import { IsNotEmpty, IsString, Validate } from "class-validator";
import { isPositiveNumber } from "../../file-parser/validators/isPositiveNumber.validator";
import { IsValidFormat } from "../validators/isValidFormat.validator";

export class GetResultsDto {
    @IsNotEmpty()
    @Validate(isPositiveNumber)
    id: number;

    @IsNotEmpty()
    @IsString()
    @Validate(IsValidFormat)
    format: string;
}