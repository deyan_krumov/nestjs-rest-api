import { Injectable, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { User } from '@prisma/client';
import { PrismaService } from '../prisma/prisma.service';
import { GetResultsDto } from './dto';
import { OutputManager } from './outputManager';

@Injectable()
export class FileOutputService {
    constructor(private prisma: PrismaService) {}

    async returnResults(dto: GetResultsDto, user: User) {
        const logFile = await this.prisma.logFile.findFirst({
            where: {
                id: dto.id,
                user: user,
            }
        });

        if(!logFile)
            throw new NotFoundException('File doesn`t exist or you are unauthorized to view it');

        let outputManager = new OutputManager();
        outputManager.setFormatStrategy(dto.format);

        return {
            result: outputManager.getResult(logFile.parsed)
        };
    }
}
