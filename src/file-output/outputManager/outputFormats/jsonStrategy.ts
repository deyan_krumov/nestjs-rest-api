import { Prisma } from "@prisma/client";
import { OutputFormatStrategy } from "../OutputManager.class";

export class JsonStrategy implements OutputFormatStrategy {
    public convert(obj: Prisma.JsonValue): string {
        return obj.toString();
    }
}