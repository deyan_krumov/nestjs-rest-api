import { ConfigService } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { PrismaService } from '../prisma/prisma.service';
import { FileOutputService } from './file-output.service';
import { OutputManager } from './outputManager';

describe('FileOutputService', () => {
  let service: FileOutputService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [FileOutputService, PrismaService, ConfigService],
    }).compile();

    service = module.get<FileOutputService>(FileOutputService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('outputManager', () => {
    it('should throw if invalid format', () => {
      expect(() => new OutputManager().setFormatStrategy('jnuidha')).toThrow('Invalid format');
    });

    it('shouldn`t throw if valid format', () => {
      expect(() => new OutputManager().setFormatStrategy('json')).not.toThrow();
    });

    describe('Strategies', () => {
      let outputManager = new OutputManager();
      const obj = {
        john_smith: 26,
        peter_johnson: 600
      }

      const json = JSON.stringify(obj);

      it('Json strategy test', () => {
        outputManager.setFormatStrategy('json');
        expect(outputManager.getResult(json)).toStrictEqual(json);
      });

      it('Html strategy test', () => {
        outputManager.setFormatStrategy('html');
        const html = '<table><tr><th>Users</th><th>Time spent in seconds</th></tr><tr><td>john_smith</td><td>26</td></tr><tr><td>peter_johnson</td><td>600</td></tr></table>';
        expect(outputManager.getResult(json)).toStrictEqual(html);
      });

      it('Xml strategy test', () => {
        outputManager.setFormatStrategy('xml');
        const xml = "<john_smith>26</john_smith><peter_johnson>600</peter_johnson>";
        expect(outputManager.getResult(json)).toStrictEqual(xml);
      });
    });
  });
});
