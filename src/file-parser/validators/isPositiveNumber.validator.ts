import { ValidatorConstraint, ValidatorConstraintInterface, ValidationArguments } from 'class-validator';

@ValidatorConstraint({ name: 'customText', async: false })
export class isPositiveNumber implements ValidatorConstraintInterface {
  validate(text: string, args: ValidationArguments) {
    let n = Number(text);
    if(n != NaN)
        return n >= 0;
    return false;
  }

  defaultMessage(args: ValidationArguments) {
    // here you can provide default error message if validation failed
    return 'Number ($value) should be positive!';
  }
}