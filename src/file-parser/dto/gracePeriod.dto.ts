import { IsNotEmpty, Validate } from "class-validator";
import { isPositiveNumber } from "../validators/isPositiveNumber.validator";

export class gracePeriodDto {
    // @IsNumber()
    @IsNotEmpty()
    @Validate(isPositiveNumber)
    // @IsPositive()
    gracePeriod: number;
}