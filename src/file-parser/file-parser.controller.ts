import { BadRequestException, Body, Controller, HttpCode, HttpStatus, Param, ParseIntPipe, Post, UploadedFiles, UseGuards, UseInterceptors, UsePipes, ValidationPipe } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { FilesInterceptor } from '@nestjs/platform-express';
import { User } from '@prisma/client';
import { GetUser } from '../auth/decorator';
import { gracePeriodDto } from './dto';
import { FileParserService } from './file-parser.service';
import { fileTypeFilter } from './multer-options-objects';

const MAX_COUNT_FILES = 5;
const MB = 1e6;
const MAX_FILE_SIZE = 5 * MB;

@UseGuards(AuthGuard('jwt')) //Uses the JwtStrategy in auth/strategy to guard this route
@Controller('file-parser')
export class FileParserController {
    constructor(private FileParserService: FileParserService) {}

    @HttpCode(HttpStatus.CREATED)
    @Post('upload-file')
    @UseInterceptors(
      FilesInterceptor('files', MAX_COUNT_FILES, {
        fileFilter: fileTypeFilter,
        limits: {fileSize: MAX_FILE_SIZE},
      })
    )
    fileParse(@UploadedFiles() files: Array<Express.Multer.File>, @Body() dto: gracePeriodDto, @GetUser() user: User) {
      if(!files) throw new BadRequestException('Should provide atleast 1 valid file');
      return this.FileParserService.fileParse(files, dto, user);
    }
}