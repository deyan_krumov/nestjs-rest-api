import { UnsupportedMediaTypeException } from '@nestjs/common';
import * as path from 'path';

const ALLOWED_FORMATS = ['.txt'];

export const fileTypeFilter = (
    req: any,
    file: { originalname: string },
    callback: (error: Error, acceptFile: boolean) => any,
) => {
    let ext = path.extname(file.originalname);
    if (!isValid(ext, ALLOWED_FORMATS)) {
        req.fileValidationError = 'Invalid file type';
        return callback(new UnsupportedMediaTypeException('Unsupported file format'), false);
    }
    return callback(null, true);
};

function isValid(ext: string, allowedFileFormats: string[]): boolean {
    for(let format of allowedFileFormats) {
        if(ext === format) return true;
    }
    return false;
}