import { Injectable } from '@nestjs/common';
import * as path from 'path';
import { gracePeriodDto } from './dto';
import { FileParserFactory } from './FileParser';
import { PrismaService } from '../prisma/prisma.service';
import { User } from '@prisma/client';

@Injectable()
export class FileParserService {

    constructor(private prisma: PrismaService) {}

    async fileParse(files: Array<Express.Multer.File>, dto: gracePeriodDto, user: User) {
        const fileNames = files.map(file => file.originalname);
        const hash = hashFileNames(fileNames);

        //Search if this files were already parsed
        const alreadyExistingFile = await this.prisma.logFile.findFirst({
            where: {
                hash: hash,
                userId: user.id
            },
        });

        if(alreadyExistingFile) 
            return {fileId: alreadyExistingFile.id};
        
        const ext = path.extname(files[0].originalname);
        const filesString = files.map(file => file.buffer.toString());

        let fileParser = FileParserFactory.getFileParser(ext, Number(dto.gracePeriod));
        const parsedFile = fileParser.parse(filesString);

        const logFile = await this.prisma.logFile.create({
            data: {
                name: fileNames.join(),
                content: filesString,
                parsed: JSON.stringify(parsedFile),
                hash: hash,
                userId: user.id
            },
        });

        return {fileId: logFile.id};
    }
}

function hashFileNames(fileNames: string[]) {
    let sorted = fileNames.sort(); //Sorts so even if the same files are given in different order it will still generate the same hash
    return stringToHash(sorted.join());
}

function stringToHash(string: string): string {     
    let hash = 0;
      
    if (string.length == 0) return String(hash);
      
    for (let i = 0; i < string.length; i++) {
        let char = string.charCodeAt(i);
        hash = ((hash << 5) - hash) + char;
        hash = hash & hash;
    }
      
    return String(hash);
}