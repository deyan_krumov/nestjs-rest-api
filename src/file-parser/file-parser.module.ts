import { Module } from '@nestjs/common';
import { FileParserController } from './file-parser.controller';
import { FileParserService } from './file-parser.service';
import { FileParserFactory } from './FileParser';

@Module({
  controllers: [FileParserController],
  providers: [FileParserService, FileParserFactory]
})
export class FileParserModule {}
