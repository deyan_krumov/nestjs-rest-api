import { INestApplication, ValidationPipe } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import { AppModule } from '../src/app.module';
import { PrismaService } from '../src/prisma/prisma.service';
import * as pactum from 'pactum'; //Library for request making
import { AuthDto } from '../src/auth/dto';
import { gracePeriodDto } from '../src/file-parser/dto';

describe('App e2e', () => {
  let app: INestApplication;
  let prisma: PrismaService;

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleRef.createNestApplication();
    app.useGlobalPipes(
      new ValidationPipe({
        whitelist: true,
      }));

      await app.init();
      await app.listen(3334);

      prisma = app.get(PrismaService);
      await prisma.cleanDb();

      pactum.request.setBaseUrl('http://localhost:3334');
  });

  afterAll(() => {
    app.close();
  });

  describe('Auth', () => {
    const dto: AuthDto = {
      email: 'test@mail.com',
      password: '123'
    };

    describe("Signup", () => {
      it("Should throw if email is empty", () => {
        return pactum.spec().post('/auth/signup').withBody({password: dto.password}).expectStatus(400);
      });

      it("Should throw if password is empty", () => {
        return pactum.spec().post('/auth/signup').withBody({email: dto.email}).expectStatus(400);
      });

      it("Should throw if body is empty", () => {
        return pactum.spec().post('/auth/signup').expectStatus(400);
      });

      it("Should throw if email is invalid", () => {
        return pactum.spec().post('/auth/signup').withBody({email: "kek"}).expectStatus(400);
      });

      it("should signup", () => {

        return pactum.spec().post('/auth/signup').withBody(dto).expectStatus(201)
        .stores('access_token', 'accessToken').expectBody({accessToken: '$S{access_token}'});
      });
    });

    describe("Signin", () => {
      it("Should throw if email is empty", () => {
        return pactum.spec().post('/auth/signin').withBody({password: dto.password}).expectStatus(400);
      });

      it("Should throw if password is empty", () => {
        return pactum.spec().post('/auth/signin').withBody({email: dto.email}).expectStatus(400);
      });

      it("Should throw if body is empty", () => {
        return pactum.spec().post('/auth/signin').expectStatus(400);
      });

      it("Should throw if email is invalid", () => {
        return pactum.spec().post('/auth/signin').withBody({email: "kek"}).expectStatus(400);
      });

      it("should signin", () => {
        return pactum.spec().post('/auth/signin').withBody(dto).expectStatus(200)
        .stores('access_token', 'accessToken').expectBody({accessToken: '$S{access_token}'});
      });
    });
  });

  // const fileDto: gracePeriodDto = {
  //   gracePeriod: 1,
  // }

  // const correctfilePath = './src/file-parser/testFiles/withLogout.txt';
  // describe('file-parser', () => {
  //   it('upload file', () => {
  //     return pactum.spec().post('/file-parser/upload-file').withHeaders({Authorization: 'Bearer $S{access_token}'})
  //     .withBody(fileDto).withFile('files', correctfilePath).expectStatus(201);
  //   });

  //   // it('It accepts valid format', () => {
  //   //   return pactum.spec().get('/file-output/results?id=$S{file_id}&format=json').withHeaders({Authorization: 'Bearer $S{access_token}'}).expectStatus(200);
  //   // });
  // });

  describe("Users", () => {
    describe('my-files', () => {
      it('should get current user files', () => {
        return pactum.spec().get('/users/my-files').withHeaders({Authorization: 'Bearer $S{access_token}'}).expectStatus(200);
      });
    });
  });
});
